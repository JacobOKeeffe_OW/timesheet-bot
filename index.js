const puppeteer = require('puppeteer');
const _ = require('lodash');
const moment = require('moment');

const auth = require('./auth.json');

// config settings
const runHeadless = process.env.NODE_ENV === 'production';

// app constants
const DATE_CELL_IX = 0;
const STATUS_CELL_IX = 3;
const EDIT_CELL_IX = 4;

const timesheetCodes = {
  // publicHoliday: 'HOL00101',
  vacation: 'VAC00101',
  personal: 'PER00101',
  sick: 'SIC00101',
};

// get Sunday of the current week
const searchDate = moment().startOf('week').format('DD-MMM-YY');

// current time
const now = moment();


/**
 * @returns list of urls for timesheets which haven't been submitted
 */
async function getUnsubmittedTimesheetUrls(rowElementHandles) {
  const urls = [];
  for (const row of rowElementHandles) {
    let rowData = await row.$$('td');
    let date = _.trim(await (await rowData[DATE_CELL_IX].getProperty('innerText')).jsonValue());
    let status = _.trim(await (await rowData[STATUS_CELL_IX].getProperty('innerText')).jsonValue());
    const anchor = await rowData[EDIT_CELL_IX].$('a');
    if (status !== 'Consolidated' && status !== 'Submitted' && moment(date, 'DD-MMM-YY').isBefore(now)) {
      urls.push(_.trim(await (await anchor.getProperty('href')).jsonValue()));
    }
  }
  return urls;
}

async function editTimesheetEntry({ page, ix, hours = [0, 0, 0, 0, 0]}) {
  // the first timesheet entry seems to be 104, second 105 etc
  const ixOffset = 4;
  const editBtnSelector = `#ctl00_content_CaseTimeGrid_ctl00_ctl0${ix+ixOffset}_gbcEditColumn`;
  const locationInputSelector = `#ctl00_content_CaseTimeGrid_ctl00_ctl0${ix+ixOffset}_LocationPicker_LocationCombo_Input`;
  const sundayEntrySelector = `#ctl00_content_CaseTimeGrid_ctl00_ctl0${ix+ixOffset}_Day1TextBox`;
  const confirmEntrySelector = `#ctl00_content_CaseTimeGrid_ctl00_ctl0${ix+ixOffset}_UpdateButton`;

  await page.$(editBtnSelector).then(editBtn => editBtn.click());
  await page.waitFor(locationInputSelector);
  await page.$(locationInputSelector).then(locationInput => locationInput.click());
  await page.keyboard.type('ireland', { delay: 10 });
  await page.waitFor(1500);
  await page.keyboard.press('ArrowDown', { delay: 10 });
  await page.keyboard.press('ArrowDown', { delay: 10 });
  await page.keyboard.press('Enter', { delay: 1000 });

  await page.focus(sundayEntrySelector);

  // enter in the hours for this entry
  for (let i = 0; i < hours.length; i++) {
    await page.keyboard.press('Tab');
    await page.keyboard.type(`${hours[i]}`);
  }

  // confirm the entry
  await page.evaluate((selector) => {
    document.querySelector(selector).click();
  }, confirmEntrySelector);
}

async function submitTimesheet({ timesheetUrl, browser }) {
  // open a new tab and navigate to the url for the particular timesheet
  const timesheetPage = await browser.newPage();
  await timesheetPage.goto(timesheetUrl);

  // hit the 'clone timesheet' button, confirm the dialog, and wait for it to complete
  const cloneTimesheetBtn = await timesheetPage.$('#ctl00_content_CloneLastTimesheetRowsButton');
  timesheetPage.on('dialog', async (dialog) => await dialog.accept());
  await cloneTimesheetBtn.click();
  await timesheetPage.waitFor('#ctl00_content_MessageBox1_MessageBoxDisplayPanel>p>input', { visible: true });

  // dismiss the modal that appears
  const cloneConfirmationModal = await timesheetPage.$('#ctl00_content_MessageBox1_MessageBoxDisplayPanel');
  const dismissBtn = await cloneConfirmationModal.$('p>input');
  await dismissBtn.click();

  // enter time off
  for (let ix = 0; ix <=2; ix++) {
    await editTimesheetEntry({ page: timesheetPage, ix });
    await timesheetPage.waitFor(2000);
  }
  const hours = [8, 8, 8, 8, 8];
  await editTimesheetEntry({ page: timesheetPage, ix: 3, hours });
  await timesheetPage.$('#ctl00_content_CloseButton');
  await timesheetPage.close();
}

puppeteer.launch({ headless: runHeadless }).then(async browser => {
  const catchError = async message => {
    console.error(message);
    await browser.close();
  };

  // navigate to main timesheet page
  const page = (await browser.pages())[0];
  try {
    await page.goto(`https://${auth.username}:${auth.password}@timereporting.myowg.com/default.aspx`);
  } catch (e) {
    return catchError(e.message);
  }

  // find the timesheet entries
  const selector = 'table#ctl00_content_HistoryGrid_ctl00> tbody > tr';
  const rows = await page.$$(selector);

  // get an array of urls for timesheets which haven't been submitted
  const timesheetUrls = await getUnsubmittedTimesheetUrls(rows);
  for (const timesheetUrl of timesheetUrls.reverse()) {
    await submitTimesheet({ timesheetUrl, browser });
  }

  await page.waitFor(5000);
  await browser.close();
});
